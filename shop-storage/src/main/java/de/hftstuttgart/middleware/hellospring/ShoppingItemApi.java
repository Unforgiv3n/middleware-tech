package de.hftstuttgart.middleware.hellospring;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class ShoppingItemApi {

    @Autowired
    private ShoppingItemRepository shoppingItemRepository;

    @GetMapping("/shoppingItem")
    public List<ShoppingItem> getAllShoppingItems() {
        return StreamSupport.stream(shoppingItemRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @GetMapping("/shoppingItem/{name}")
    public String getAll(@PathVariable String name) {
        shoppingItemRepository.save(new ShoppingItem(name));
        return "Shopping Item with " + name + " was created";
    }
}
