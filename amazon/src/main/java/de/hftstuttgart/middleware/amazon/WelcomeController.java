package de.hftstuttgart.middleware.amazon;

import de.hftstuttgart.middleware.amazon.model.ShoppingItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class WelcomeController {

    @Autowired
    ShopStorageClient shopStorageClient;

    // inject via application.properties
    @Value("${welcome.message}")
    private String message;

    @GetMapping("/")
    public String main(Model model) {
        List<ShoppingItem> productList = shopStorageClient.getProductList();
        List<String> names = new ArrayList<>();
        for (ShoppingItem item: productList) {
            names.add(item.getName());
        }
        model.addAttribute("message", message);
        model.addAttribute("tasks", names);

        return "welcome"; //view
    }

    @GetMapping("/hello")
    public String mainWithParam(@RequestParam(name = "name", required = false, defaultValue = "") String name,
            Model model) {

        model.addAttribute("message", name);

        return "welcome"; //view
    }
}