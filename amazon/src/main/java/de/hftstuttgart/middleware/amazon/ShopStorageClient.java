package de.hftstuttgart.middleware.amazon;

import de.hftstuttgart.middleware.amazon.model.ShoppingItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class ShopStorageClient {

    @Autowired
    RestTemplate restTemplate;

    @Value("${SHOP_STORAGE_URL}")
    private String url;

    @RequestMapping(value = "/template/welcome")
    public List<ShoppingItem> getProductList() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<ArrayList<ShoppingItem>> entity = new HttpEntity<>(headers);

        ParameterizedTypeReference<List<ShoppingItem>> typeRef = new ParameterizedTypeReference<>() {
        };

        return restTemplate.exchange(url + "/shoppingItem", HttpMethod.GET, entity, typeRef).getBody();
    }
}
